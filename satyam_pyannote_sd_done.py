
!pip install -qq pyannote.audio==2.1.1
!pip install -qq rich

# # download AMI-SDM mini corpus
# # !pip install -qq pyannote.audio==2.1.1
# # !pip install -qq rich
# %cd /content/
# !git clone https://github.com/pyannote/AMI-diarization-setup
# %cd /content/AMI-diarization-setup/pyannote/
# !bash downloadamisdmmini.sh
# !PYANNOTEDATABASECONFIG="/content/custom_database.yml" pyannote-database info AMI-SDM.SpeakerDiarization.mini

!pip install protobuf==3.20.0
!pip install torch torchvision torchaudio -f https://download.pytorch.org/whl/cu102/torchstable.html
!pip install torchtext
!pip install pyannote.database

!git clone https://github.com/pyannote/AMI-diarization-setup

!PYANNOTE_DATABASE_CONFIG="/content/custom_database.yml" pyannote-database info AMI-SDM.SpeakerDiarization.mini

import os
import random
import shutil
import pandas as pd

# Custom folders
basefolder = '/content/'
wavfolder = '/content/wav_folder'
csvfolder = '/content/csv_folder'
trainminipath = 'train.mini.txt'
listsfolder = '/content/lists'
rttmsbasepath = 'only_words/rttms/'
uemsbasepath = 'uems/'

# Create directories
os.makedirs(listsfolder, exist_ok=True)

# List all files in the folder
files = os.listdir('/content/csv_folder/')

# Iterate through each file
for file_name in files:
    # Check if the file is a CSV file
    if file_name.endswith('.csv'):
        # Construct the old and new file paths
        old_path = os.path.join('/content/csv_folder/', file_name)
        new_name = file_name.replace('decodedir_', '')
        new_path = os.path.join('/content/csv_folder/', new_name)

        # Rename the file
        os.rename(old_path, new_path)

print("Files renamed successfully.")

# Get filenames from wavfolder
wavfiles = [file.replace('.wav', '') for file in os.listdir('/content/wav_folder/') if file.endswith('.wav')]

# Shuffle the list of URIs to randomize the order
random.shuffle(wavfiles)
random.shuffle(wavfiles)
# Define the proportions for training, development, and testing sets
train_ratio = 0.5
dev_ratio = 0.25
test_ratio = 0.25

# Calculate the number of samples for each set
total_samples = len(wavfiles)
train_samples = int(train_ratio * total_samples)
dev_samples = int(dev_ratio * total_samples)
test_samples = total_samples - train_samples - dev_samples

# Split the list of URIs into training, development, and testing sets
train_uris = wavfiles[:train_samples]
dev_uris = wavfiles[train_samples:train_samples + dev_samples]
test_uris = wavfiles[train_samples + dev_samples:]

# Print the contents of the splits
print("train_uris:", train_uris)
print("dev_uris:", dev_uris)
print("test_uris:", test_uris)

# Write the filenames to train.mini.txt
fulltrainminipath = os.path.join(listsfolder, 'train.mini.txt')
with open(fulltrainminipath, 'w') as trainminifile:
    trainminifile.write('\n'.join(train_uris))

# Print the content of train.mini.txt after writing
print("Contents of train.mini.txt after writing:")
with open(os.path.join(listsfolder, 'train.mini.txt'), 'r') as f:
    train_uris_after_write = [line.strip() for line in f]
print(train_uris_after_write)

# Create development.mini.txt
with open(os.path.join(listsfolder, 'development.mini.txt'), 'w') as developmentfile:
    developmentfile.write('\n'.join(dev_uris))

# Print the contents of development.mini.txt after writing
print("Contents of development.mini.txt after writing:")
with open(os.path.join(listsfolder, 'development.mini.txt'), 'r') as f:
    development_uris_after_write = [line.strip() for line in f]
print(development_uris_after_write)

# Create test.mini.txt
with open(os.path.join(listsfolder, 'test.mini.txt'), 'w') as testfile:
    testfile.write('\n'.join(test_uris))

# Print the contents of test.mini.txt after writing
print("Contents of test.mini.txt after writing:")
with open(os.path.join(listsfolder, 'test.mini.txt'), 'r') as f:
    test_uris_after_write = [line.strip() for line in f]
print(test_uris_after_write)

# Generate paths and content for RTTM and UEM files for each set (train, development, test)
for settype, uris_set in [('train', train_uris), ('development', dev_uris), ('test', test_uris)]:
    rttmspath = os.path.join(rttmsbasepath, settype)
    uemspath = os.path.join(uemsbasepath, settype)

    os.makedirs(os.path.join(basefolder, rttmspath), exist_ok=True)
    os.makedirs(os.path.join(basefolder, uemspath), exist_ok=True)

    for uri in uris_set:
        csvpath = os.path.join(basefolder, csvfolder, f'{uri}.csv')
        rttmpath = os.path.join(basefolder, rttmspath, f'{uri}.rttm')
        uempath = os.path.join(basefolder, uemspath, f'{uri}.uem')

        # Read CSV file
        df = pd.read_csv(csvpath)

        # Create content for RTTM file
        with open(rttmpath, 'w') as rttmfile:
            for index, row in df.iterrows():
                rttmfile.write(
                    f"SPEAKER {uri} 1 {row['start']} {row['stop'] - row['start']} <NA> <NA> {row['speaker']} <NA> <NA>\n"
                )

        # Create content for UEM file
        with open(uempath, 'w') as uemfile:
            uemfile.write(f'{uri} 1 0.000 {df["stop"].max()}\n')

print("RTTM and UEM files created successfully.")

# import yaml

# data = {
#     'Databases': {
#         'SAT': '/content/wav_folder/{uri}.wav',
#         'SAT-YAM': '/content/wav_folder/{uri}.wav'
#     },
#     'Protocols': {
#         'SAT-YAM': {
#             'SpeakerDiarization': {
#                 'mini': {
#                     'train': {
#                         'uri': '/content/lists/train.mini.txt',
#                         'annotation': '/content/only_words/rttms/train/{uri}.rttm',
#                         'annotated': '/content/uems/train/{uri}.uem'
#                     },
#                     'development': {
#                         'uri': '/content/lists/development.mini.txt',
#                         'annotation': '/content/only_words/rttms/development/{uri}.rttm',
#                         'annotated': '/content/uems/development/{uri}.uem'
#                     },
#                     'test': {
#                         'uri': '/content/lists/test.mini.txt',
#                         'annotation': '/content/only_words/rttms/test/{uri}.rttm',
#                         'annotated': '/content/uems/test/{uri}.uem'
#                     }
#                 }
#             }
#         }
#     }
# }

# with open('/content/custom_database.yml', 'w') as file:
#     yaml.dump(data, file)

# print("custom_database.yml has been created successfully.")

import yaml

data = {
    'Databases': {
        'AMI': '/content/wav_folder/{uri}.wav',
        'AMI-SDM': '/content/wav_folder/{uri}.wav'
    },
    'Protocols': {
        'AMI-SDM': {
            'SpeakerDiarization': {
                'mini': {
                    'train': {
                        'uri': '/content/lists/train.mini.txt',
                        'annotation': '/content/only_words/rttms/train/{uri}.rttm',
                        'annotated': '/content/uems/train/{uri}.uem'
                    },
                    'development': {
                        'uri': '/content/lists/development.mini.txt',
                        'annotation': '/content/only_words/rttms/development/{uri}.rttm',
                        'annotated': '/content/uems/development/{uri}.uem'
                    },
                    'test': {
                        'uri': '/content/lists/test.mini.txt',
                        'annotation': '/content/only_words/rttms/test/{uri}.rttm',
                        'annotated': '/content/uems/test/{uri}.uem'
                    }
                }
            }
        }
    }
}

with open('/content/custom_database.yml', 'w') as file:
    yaml.dump(data, file)

print("custom_database.yml has been created successfully.")

import os
os.environ["PYANNOTE_DATABASE_CONFIG"] = "/content/custom_database.yml"
from pyannote.database import get_protocol, FileFinder
dataset = get_protocol("AMI-SDM.SpeakerDiarization.mini", {"audio": FileFinder()})

# for batch in dataset:
#     inputs, targets = batch
#     print("Input shape:", inputs.shape)
#     print("Target shape:", targets.shape)

from huggingface_hub import notebook_login
notebook_login()
#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

from pyannote.audio import Pipeline
pretrained_pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization", use_auth_token=True)

from pyannote.audio import Model
model = Model.from_pretrained("pyannote/segmentation", useauthtoken=True)

# # this takes approximately 2min to run on Google Colab GPU
# from pyannote.metrics.diarization import DiarizationErrorRate
# metric = DiarizationErrorRate()

# for file in dataset.test():
#     # apply pretrained pipeline
#     file["pretrained pipeline"] = pretrained_pipeline(file)

#     # evaluate its performance
#     metric(file["annotation"], file["pretrained pipeline"], uem=file["annotated"])

# print(f"The pretrained pipeline reaches a Diarization Error Rate (DER) of {100 * abs(metric):.1f}% on {dataset.name} test set.")

len(model.specifications.classes)



from pyannote.audio.tasks import Segmentation
task = Segmentation(
    dataset,
    duration=model.specifications.duration,
    max_num_speakers=2,
    batch_size=2,
    num_workers=2,
    loss="bce",
    vad_loss="bce")
model.task = task
model.setup(stage="fit")

# !rm -rf '/content/lightning_logs'

# this takes approximately 15min to run on Google Colab GPU
from types import MethodType
from torch.optim import Adam
from pytorch_lightning.callbacks import (
    EarlyStopping,
    ModelCheckpoint,
    RichProgressBar,
)

# we use Adam optimizer with 1e-4 learning rate
def configure_optimizers(self):
    return Adam(self.parameters(), lr=1e-4)

model.configure_optimizers = MethodType(configure_optimizers, model)

# we monitor diarization error rate on the validation set
# and use to keep the best checkpoint and stop early
monitor, direction = task.val_monitor
checkpoint = ModelCheckpoint(
    monitor=monitor,
    mode=direction,
    save_top_k=1,
    every_n_epochs=1,
    save_last=False,
    save_weights_only=False,
    filename="{epoch}",
    verbose=False,
)
early_stopping = EarlyStopping(
    monitor=monitor,
    mode=direction,
    min_delta=0.0,
    patience=10,
    strict=True,
    verbose=False,
)

callbacks = [RichProgressBar(), checkpoint, early_stopping]

# we train for at most 20 epochs (might be shorter in case of early stopping)
from pytorch_lightning import Trainer
trainer = Trainer(accelerator="gpu",
                  callbacks=callbacks,
                  max_epochs=5,
                  gradient_clip_val=0.5)
trainer.fit(model)

!rm -rf '/content/lightning_logs'
!rm -rf '/content/lists'
!rm -rf '/content/only_words'
!rm -rf '/content/uems'

!rm -rf ''

finetuned_model = checkpoint.best_model_path

pretrained_hyperparameters = pretrained_pipeline.parameters(instantiated=True)
pretrained_hyperparameters

# this takes approximately 5min to run on Google Colab GPU
from pyannote.audio.pipelines import SpeakerDiarization
from pyannote.pipeline import Optimizer

pipeline = SpeakerDiarization(
    segmentation=finetuned_model,
    clustering="OracleClustering",
    # max_speakers=2
)
# as reported in the technical report, min_duration_off can safely be set to 0.0
pipeline.freeze({"segmentation": {"min_duration_off": 0.0}})

optimizer = Optimizer(pipeline)
dev_set = list(dataset.development())

iterations = optimizer.tune_iter(dev_set, show_progress=False)
best_loss = 1.0
for i, iteration in enumerate(iterations):
    print(f"Best segmentation threshold so far: {iteration['params']['segmentation']['threshold']}")
    if i > 20: break   # 50 iterations should give slightly better results

best_segmentation_threshold = optimizer.best_params["segmentation"]["threshold"]

# this takes approximately 5min to run on Google Colab GPU
pipeline = SpeakerDiarization(
    segmentation=finetuned_model,
    embedding=pretrained_pipeline.embedding,
    embedding_exclude_overlap=pretrained_pipeline.embedding_exclude_overlap,
    clustering=pretrained_pipeline.klustering,
    # max_num_speakers=2s
)

pipeline.freeze({
    "segmentation": {
        "threshold": best_segmentation_threshold,
        "min_duration_off": 0.0,
    },
    "clustering": {
        "method": "centroid",
        "min_cluster_size": 15,
    },
})

optimizer = Optimizer(pipeline)
iterations = optimizer.tune_iter(dev_set, show_progress=False)
best_loss = 1.0
for i, iteration in enumerate(iterations):
    print(f"Best clustering threshold so far: {iteration['params']['clustering']['threshold']}")
    if i > 50: break  # 50 iterations should give slightly better results

best_clustering_threshold = optimizer.best_params['clustering']['threshold']

# this takes approximately 2min to run on Google Colab GPU
finetuned_pipeline = SpeakerDiarization(
    segmentation=finetuned_model,
    embedding=pretrained_pipeline.embedding,
    embedding_exclude_overlap=pretrained_pipeline.embedding_exclude_overlap,
    clustering=pretrained_pipeline.klustering,
    # max_speakers=2
)

finetuned_pipeline.instantiate({
    "segmentation": {
        "threshold": best_segmentation_threshold,
        "min_duration_off": 0.0,
    },
    "clustering": {
        "method": "centroid",
        "min_cluster_size": 15,
        "threshold": best_clustering_threshold,
    },
})

# metric = DiarizationErrorRate()

# for file in dataset.test():
#     # apply finetuned pipeline
#     file["finetuned pipeline"] = finetuned_pipeline(file)

#     # evaluate its performance
#     metric(file["annotation"], file["finetuned pipeline"], uem=file["annotated"])

# print(f"The finetuned pipeline reaches a Diarization Error Rate (DER) of {100 * abs(metric):.1f}% on {dataset.name} test set.")

pretrained_hyperparameters = pipeline.parameters(instantiated=True)
pretrained_hyperparameters

from pyannote.core import Segment
from pyannote.audio.pipelines import SpeakerDiarization

# Replace 'your_unseen_audio_file.wav' with the path to your unseen audio file
unseen_audio_path = '/content/wav_folder/20230502-180814_38690029_10061936_HEALTH09.wav'

# Instantiate the SpeakerDiarization pipeline
# finetuned_pipeline = SpeakerDiarization.from_config_file(finetuned_model)

# Load the unseen audio file
unseen_file = {'uri': 'unseen', 'file': unseen_audio_path}

# Apply the finetuned pipeline to the unseen audio file
diarization_result = finetuned_pipeline.apply(unseen_audio_path, num_speakers=2)

# Print the diarization result
print(diarization_result)

finetuned_pipeline.apply('/content/wav_folder/20230502-180814_38690029_10061936_HEALTH09.wav',num_speakers=2)

for file in dataset.development():
    print(file)
    # apply finetuned pipeline
    print(finetuned_pipeline(file,num_speakers=2))

# 1. visit hf.co/pyannote/speaker-diarization and accept user conditions
# 2. visit hf.co/pyannote/segmentation and accept user conditions
# 3. visit hf.co/settings/tokens to create an access token
# 4. instantiate pretrained speaker diarization pipeline
from pyannote.audio import Pipeline
pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization@2.1",
                                    use_auth_token=True)

# 1. visit hf.co/pyannote/speaker-diarization and accept user conditions
# 2. visit hf.co/pyannote/segmentation and accept user conditions
# 3. visit hf.co/settings/tokens to create an access token
# 4. instantiate pretrained speaker diarization pipeline
from pyannote.audio import Pipeline
pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization@2.1",
                                    use_auth_token=True)

diarization = pipeline("/content/wav_folder/20230502-180814_38690029_10061936_HEALTH09.wav", num_speakers=2)

diarization

from pyannote.audio import Pipeline
pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization@2.1",
                                    use_auth_token='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')

diarization = pipeline("/content/wav_folder/20230502-180814_38690029_10061936_HEALTH09.wav", num_speakers=2)
with open("audio.rttm", "w") as rttm:
    diarization.write_rttm(rttm)

from pyannote.audio import Pipeline
pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization@2.1",
                                    use_auth_token='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')

diarization = pipeline("/content/wav_folder/20230502-180814_38690029_10061936_HEALTH09.wav", num_speakers=2)
with open("audio.rttm", "w") as rttm:
    diarization.write_rttm(rttm)
import json

def convert_rttm_to_json(rttm_lines):
    speaker_segments = {}

    for line in rttm_lines:
        parts = line.split()
        recording_id = parts[1]
        start_time = float(parts[3])
        stop_time = start_time + float(parts[4])
        speaker_id = parts[7]
        speaker_id = speaker_id.split('_')[1][1]

        if speaker_id not in speaker_segments:
            speaker_segments[speaker_id] = []

        speaker_segments[speaker_id].append({"start": start_time, "stop": stop_time})

    return json.dumps(speaker_segments, indent=2)

# Example usage with your provided lines
rttm_lines = []
file_path = '/content/audio.rttm'
with open(file_path, 'r') as file:
    rttm_lines.extend(file.readlines())

json_result = convert_rttm_to_json(rttm_lines)
print(json_result)

import yaml

data = {
    'Databases': {
        'AMI': '/content/wav_folder/{uri}.wav',
        'AMI-SDM': '/content/wav_folder/{uri}.wav'
    },
    'Protocols': {
        'AMI-SDM': {
            'SpeakerDiarization': {
                'mini': {
                    'development': {
                        'uri': '/content/lists/development.mini.txt',
                        'annotated': '/content/uems/development/{uri}.uem'
                        }
            }
        }
    }
}
}
with open('/content/new_custom_database.yml', 'w') as file:
    yaml.dump(data, file)

print("custom_database.yml has been created successfully.")

import os
os.environ["PYANNOTE_DATABASE_CONFIG"] = "/content/new_custom_database.yml"
from pyannote.database import get_protocol, FileFinder
dataset = get_protocol("AMI-SDM.SpeakerDiarization.mini", {"audio": FileFinder()})

# Specify the output file path
output_file_path = 'output.txt'

# Open the file in write mode
with open(output_file_path, 'w') as output_file:
    # Iterate over each file in the development dataset
    for file in dataset.development():
        # Apply the finetuned pipeline to the file
        diarization_result = finetuned_pipeline.apply(file, num_speakers=2)

        # Convert the diarization result to a string representation
        diarization_str = str(diarization_result)

        # Write the diarization result to the output file
        output_file.write(f"{file['uri']}:\n{diarization_str}\n\n")

for file in dataset.development():
  print(finetuned_pipeline.apply(file,num_speakers=2))

